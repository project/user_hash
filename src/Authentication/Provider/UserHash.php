<?php

namespace Drupal\user_hash\Authentication\Provider;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserAuthInterface;
use Drupal\user_hash\Services;
use Symfony\Component\HttpFoundation\Request;

/**
 * User Hash authentication provider.
 */
class UserHash implements AuthenticationProviderInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The user auth service.
   *
   * @var \Drupal\user\UserAuthInterface
   */
  protected UserAuthInterface $userAuth;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected FloodInterface $flood;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The user hash services.
   *
   * @var \Drupal\user_hash\Services
   */
  protected Services $services;

  /**
   * Constructs a User Hash authentication provider object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\user\UserAuthInterface $user_auth
   *   The user authentication service.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\user_hash\Services $services
   *   The user hash services.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UserAuthInterface $user_auth, FloodInterface $flood, EntityTypeManagerInterface $entity_type_manager, Services $services) {
    $this->configFactory = $config_factory;
    $this->userAuth = $user_auth;
    $this->flood = $flood;
    $this->entityTypeManager = $entity_type_manager;
    $this->services = $services;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request): bool {
    $username = $request->headers->get('X_USER_NAME');
    $hash = $request->headers->get('X_USER_HASH');
    return isset($username, $hash);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request): AccountInterface|array {
    $flood_config = $this->configFactory->get('user.flood');
    $username = $request->headers->get('X_USER_NAME');
    $hash = $request->headers->get('X_USER_HASH');
    if ($this->flood->isAllowed('user_hash.failed_login_ip', $flood_config->get('ip_limit'), $flood_config->get('ip_window'))) {
      try {
        $account = $this->services->validateHash($username, $hash);
      }
      catch (InvalidPluginDefinitionException | PluginNotFoundException) {
        return [];
      }
      if ($account) {
        if ($flood_config->get('uid_only')) {
          $identifier = $account->id();
        }
        else {
          $identifier = $account->id() . '-' . $request->getClientIP();
        }
        if ($this->flood->isAllowed('user_hash.failed_login_user', $flood_config->get('user_limit'), $flood_config->get('user_window'), $identifier)) {
          if ($this->services->isHashValid($account->id())) {
            $this->flood->clear('user_hash.failed_login_user', $identifier);
            return $account;
          }
          $this->flood->register('user_hash.failed_login_user', $flood_config->get('user_window'), $identifier);
        }
      }
    }
    $this->flood->register('user_hash.failed_login_ip', $flood_config->get('ip_window'));
    return [];
  }

}
