<?php

namespace Drupal\user_hash\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserData;
use Drupal\user_hash\Services;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generate user hashes.
 *
 * @Action(
 *   id = "user_generate_user_hash_action",
 *   label = @Translation("Generate hash for the selected user(s)"),
 *   type = "user"
 * )
 */
final class GenerateUserHash extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The user hash services.
   *
   * @var \Drupal\user_hash\Services
   */
  protected Services $userHashService;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserData
   */
  protected UserData $userData;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->userHashService = $container->get('user_hash.services');
    $instance->userData = $container->get('user.data');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(?AccountInterface $account = NULL): void {
    if ($account === NULL) {
      return;
    }
    $hash = $this->userHashService->generateHash();
    $this->userData->set('user_hash', $account->id(), 'hash', $hash);
    Cache::invalidateTags(['user:' . $account->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, ?AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\user\UserInterface $object */
    $access = $object->access('edit', $account, TRUE)
      ->andIf($object->access('update', $account, TRUE))
      ->andIf($object->hasPermission('use user_hash') ? AccessResult::allowed() : AccessResult::forbidden());

    return $return_as_object ? $access : $access->isAllowed();
  }

}
