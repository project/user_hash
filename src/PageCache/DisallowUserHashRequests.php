<?php

namespace Drupal\user_hash\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cache policy for pages served from user hash.
 *
 * This policy disallows caching of requests that use user_hash for security
 * reasons. Otherwise responses for authenticated requests can get into the
 * page cache and could be delivered to unprivileged users.
 */
class DisallowUserHashRequests implements RequestPolicyInterface {

  /**
   * {@inheritdoc}
   */
  public function check(Request $request): ?string {
    $username = $request->headers->get('X_USER_NAME');
    $hash = $request->headers->get('X_USER_HASH');
    if (isset($username, $hash)) {
      return static::DENY;
    }
    return NULL;
  }

}
