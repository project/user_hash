<?php

namespace Drupal\user_hash;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\UserDataInterface;

/**
 * Provides a User Hash services..
 */
class Services {

  /**
   * PHP hash algorithm to use.
   *
   * @var string
   */
  protected string $algorithm;

  /**
   * Quantity of characters to use for the random value at hash generation.
   *
   * @var int
   */
  protected int $randomBytes;

  /**
   * The contact settings config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected UserDataInterface $userData;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Contains the validation results indexed by user ID.
   *
   * @var array
   */
  protected array $validatedUserHashes = [];

  /**
   * Constructs a ContactPageAccess instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    UserDataInterface $user_data,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    $this->configFactory = $config_factory;
    $this->userData = $user_data;
    $this->entityTypeManager = $entity_type_manager;

    $config = $config_factory->get('user_hash.settings');
    $this->algorithm = $config->get('algorithm') ?? 'sha256';
    $this->randomBytes = (int) ($config->get('random_bytes') ?? '32');
  }

  /**
   * Generate hash.
   *
   * @param bool $raw_output
   *   When set to TRUE outputs raw binary data. FALSE outputs lowercase hex.
   *
   * @return string
   *   Returns the generated hash.
   *
   * @throws \Random\RandomException
   */
  public function generateHash(bool $raw_output = FALSE): string {
    return hash($this->algorithm, random_bytes($this->randomBytes), $raw_output);
  }

  /**
   * Validates that the given hash belongs to the given user.
   *
   * @param string $username
   *   The user name.
   * @param string $hash
   *   The hash.
   * @param bool $active_only
   *   Indicates whether only active or all users should be validated.
   *
   * @return \Drupal\Core\Session\AccountInterface|null
   *   The user account matching the user name if the hash is valid, NULL
   *   otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function validateHash(string $username, string $hash, bool $active_only = TRUE): ?AccountInterface {
    $accounts = $this->entityTypeManager->getStorage('user')
      ->loadByProperties(['name' => $username, 'status' => (int) $active_only]);
    $account = reset($accounts);
    if (empty($account)) {
      return NULL;
    }
    if ($storedHash = $this->userData->get('user_hash', $account->id(), 'hash')) {
      $this->validatedUserHashes['a' . $account->id()] = hash_equals($storedHash, $hash);
    }
    else {
      $this->validatedUserHashes['a' . $account->id()] = FALSE;
    }
    return $account;
  }

  /**
   * Returns the result of a previous validation.
   *
   * @param int $uid
   *   The user ID.
   *
   * @return bool
   *   The hash validation result.
   */
  public function isHashValid(int $uid): bool {
    return $this->validatedUserHashes['a' . $uid] ?? FALSE;
  }

}
