<?php

namespace Drupal\Tests\user_hash\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test user hash settings form.
 *
 * @group user_hash
 *
 * @requires user
 */
class UserHashSettingsFormTest extends BrowserTestBase {

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administrate user hashes.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['system', 'user', 'user_hash'];

  /**
   * Set up test environment.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
      'administer users',
      'administer account settings',
      'access user profiles',
    ]);
  }

  /**
   * Test user hash settings form.
   */
  public function testUserHashSettingsForm(): void {
    $this->drupalLogin($this->adminUser);

    $edit = [
      'algorithm' => 'sha1',
      'random_bytes' => 48,
    ];
    $path = 'admin/config/people/user_hash';
    $this->drupalGet($path);
    $this->submitForm($edit, t('Save configuration'));
    $this->drupalGet($path);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldValueEquals('algorithm', 'sha1');
    $this->assertSession()->fieldValueEquals('random_bytes', '48');
  }

}
