<?php

namespace Drupal\Tests\user_hash\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Test user hash generation.
 *
 * @group user_hash
 *
 * @requires user
 */
class UserHashGenerationTest extends BrowserTestBase {

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to administrate user hashes.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * A user without the 'use user hash' permission.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $baseUser;

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['system', 'views', 'user', 'user_hash'];

  /**
   * Set up test environment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'access administration pages',
      'administer users',
      'administer account settings',
      'access user profiles',
      'use user_hash',
    ]);
    $this->baseUser = $this->drupalCreateUser([
      'access user profiles',
    ]);
  }

  /**
   * Test user hash generation.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testUserHashGeneration(): void {
    $this->drupalLogin($this->adminUser);
    $edit = [
      'action' => 'user_generate_user_hash_action',
      // Selects adminUser.
      'user_bulk_form[1]' => TRUE,
    ];
    $path = 'admin/people';
    $this->drupalGet($path);
    $this->submitForm($edit, t('Apply to selected items'), 'views-form-user-admin-people-page-1');
    $this->assertSession()->pageTextContains('Generate hash for the selected user(s) was applied to');
    $this->drupalGet('user/' . $this->adminUser->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseMatches('/Hash<\/h4> [0-9A-Fa-f]{64}/');

    $edit = [
      'action' => 'user_generate_user_hash_action',
      // Selects baseUser.
      'user_bulk_form[0]' => TRUE,
    ];
    $this->drupalGet($path);
    $this->submitForm($edit, t('Apply to selected items'), 'views-form-user-admin-people-page-1');
    $this->drupalGet('user/' . $this->baseUser->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotContains('css', 'h4', 'Hash');
    $this->drupalLogout();

    $this->drupalLogin($this->baseUser);
    $this->drupalGet('user/' . $this->adminUser->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotContains('css', 'h4', 'Hash');
    $this->drupalLogout();

    $this->drupalLogin($this->adminUser);
    $edit = [
      'action' => 'user_delete_user_hash_action',
      'user_bulk_form[1]' => TRUE,
    ];
    $this->drupalGet($path);
    $this->submitForm($edit, t('Apply to selected items'), 'views-form-user-admin-people-page-1');
    $this->assertSession()->pageTextContains('Delete hash from the selected user(s) was applied to');
    $this->drupalGet('user/' . $this->adminUser->id());
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementNotContains('css', 'h4', 'Hash');
  }

}
